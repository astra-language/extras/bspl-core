package astra.bspl;

public class InvalidProtocolException extends RuntimeException {
    public InvalidProtocolException(String msg, Exception e) { 
        super(msg, e);
    }
    
    public InvalidProtocolException(String msg) {
        super(msg);
    }
}
