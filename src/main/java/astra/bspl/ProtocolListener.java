package astra.bspl;

public interface ProtocolListener {
    void protocolEnacted(ProtocolExecutor executor);
    void protocolActivated(ProtocolExecutor executor);
}
