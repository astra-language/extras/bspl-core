package astra.bspl;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

import astra.bspl.model.Message;
import astra.bspl.model.Protocol;

public class ProtocolLibrary {
    private static ObjectMapper mapper = new ObjectMapper();

    private Map<String, Protocol> protocols = new HashMap<>();

    public ProtocolLibrary addProtocol(Protocol protocol) {
        protocols.put(protocol.getName(), protocol);
        return this;
    }

    public Protocol getProtocol(String name) {
        return protocols.get(name);
    }

    public ProtocolLibrary loadProtocol(String filename) {
        try {
            Protocol protocol = mapper.readValue(new File(filename), Protocol.class);
            protocols.put(protocol.getName(), protocol);
        } catch (Exception e) {
            throw new InvalidProtocolException("Could not load protocol: " + filename, e);
        }
        return this;
    }

    public Protocol retrieveProtocol(Message message) {
        for (Protocol protocol : protocols.values()) {
            if (Utils.matchToSchema(protocol.getSchemas(), message) != null)
                return protocol;
        }
        return null;
    }
}
