package astra.bspl;

public class InvalidMessageException extends RuntimeException {
    public InvalidMessageException(String msg) {
        super(msg);
    }
    
}
