package astra.bspl.transport;

import java.util.HashMap;

import astra.bspl.ProtocolAdaptor;
import astra.bspl.model.Message;
import astra.bspl.model.ParameterSet;

public class LocalProtocolTransport implements ProtocolTransport {
    static HashMap<String, ProtocolAdaptor> adaptors = new HashMap<>();
    
    public void bind(ProtocolAdaptor adaptor) {
        adaptors.put(adaptor.getName(), adaptor);
    }

    @Override
    public void send(Message message) {
        ProtocolAdaptor adaptor = adaptors.get(message.getTo());
        if (adaptor == null) {
            System.out.println("Could not locate receiver: " + message.getTo());
        } else {
            adaptor.receive(message);
        }
    }

    @Override
    public void configure(ParameterSet set) {
        // No parameter configuration supported!
    }  
}
