package astra.bspl.transport;

import astra.bspl.ProtocolAdaptor;
import astra.bspl.model.Message;
import astra.bspl.model.ParameterSet;

public interface ProtocolTransport {
    void send(Message message);
    void bind(ProtocolAdaptor adaptor);
    void configure(ParameterSet set);
}
