package astra.bspl.transport;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.MulticastSocket;
import java.net.UnknownHostException;

import astra.bspl.ProtocolAdaptor;
import astra.bspl.model.Message;
import astra.bspl.model.ParameterSet;

public class UDPProtocolTransport implements ProtocolTransport {
    private ProtocolAdaptor adaptor;
	private int port;
	private String hostname;
	private InetAddress address;
	private MulticastSocket socket;
	private boolean terminating;
    private static final String IPv6_ADDRESS = "FF7E:230::1234";
    
    public void bind(ProtocolAdaptor adaptor) {
        System.out.println("Binding Adaptor to transport: " + adaptor.getName());
        this.adaptor = adaptor;
    }

    public void configure(ParameterSet set) {
        hostname = set.get("hostname", IPv6_ADDRESS);
        port = Integer.parseInt(set.get("port", "2000"));
        start();
    }
    
    @Override
    public void send(Message message) {
        try {
            ByteArrayOutputStream baout = new ByteArrayOutputStream();
            ObjectOutputStream out = new ObjectOutputStream(baout);
            out.writeObject(message);
            byte[] buf = baout.toByteArray();
            socket.send(new DatagramPacket(buf, buf.length, this.address, port));
            out.close();
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

	public void start() {
		try {
			address = InetAddress.getByName(hostname);
			if (address.isMulticastAddress()) {
				socket = new MulticastSocket();
				;
				socket.joinGroup(address);
			}
			
            System.out.println("UDP Multicast on: " + address);
			new Thread() {
				public void run() {
					try {
						MulticastSocket socket = new MulticastSocket(port);
						socket.joinGroup(address);
			            while (!terminating) {
			                try {
			                    byte[] buf = new byte[64000];
			                    DatagramPacket packet = new DatagramPacket(buf, buf.length);
			                    socket.receive(packet);
			                    ObjectInputStream in = new ObjectInputStream(new ByteArrayInputStream(buf));
                                Message message = Message.class.cast(in.readObject());
                                System.out.println("received: " + message);
			        			adaptor.receive(message);
			                    in.close();
			                } catch (IOException e) {
			                    e.printStackTrace();
			                } catch (ClassNotFoundException e) {
			        			e.printStackTrace();
			        		}
			            }
			            socket.close();
					} catch (IOException e1) {
						e1.printStackTrace();
					}
		        }
			}.start();
			
			System.out.println("["+getClass().getCanonicalName()+"] Service Started...");
		} catch (UnknownHostException uhe) {
			System.err.println("["+getClass().getCanonicalName()+"] Unknown host: " + hostname);
		} catch (IOException ioe) {
			System.err.println("["+getClass().getCanonicalName()+"] Error occurred setting up connection for: " + hostname);
            ioe.printStackTrace();
		}
	}

}
