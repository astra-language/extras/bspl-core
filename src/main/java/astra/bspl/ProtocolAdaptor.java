package astra.bspl;

import java.util.LinkedList;
import java.util.List;

import astra.bspl.model.Message;
import astra.bspl.model.ParticipantSet;
import astra.bspl.model.Protocol;
import astra.bspl.transport.ProtocolTransport;

public class ProtocolAdaptor {
    private List<ProtocolExecutor> executors = new LinkedList<>();
    private List<ProtocolExecutor> active = new LinkedList<>();

    private String name;
    private ProtocolLibrary library;
    private ProtocolListener listener;
    private ProtocolTransport transport;

    public ProtocolAdaptor(String name, ProtocolLibrary library, ProtocolTransport transport) {
        this.library = library;
        this.transport = transport;
        this.name = name;

        transport.bind(this);
    }

    public String getName() {
        return name;
    }

    public ProtocolAdaptor setListener(ProtocolListener listener) {
        this.listener = listener;
        return this;
    }

    public ProtocolExecutor enactProtocol(String protocolName, String role, ParticipantSet participants) {
        Protocol protocol = library.getProtocol(protocolName);
        if (protocol == null)
            throw new InvalidProtocolException("Attempt to enact protocol that does not exist");

        ProtocolExecutor executor = new ProtocolExecutor(name, role, participants, protocol, transport);
        executors.add(executor);

        if (executor.isActiveRole()) {
            active.add(executor);
            listener.protocolEnacted(executor);
        }

        return executor;
    }

    public void receive(Message message) {
        // Match against existing conversations
        for (ProtocolExecutor executor : executors) {
            if (executor.receive(message)) {
                if (executor.isActiveRole()) {
                    active.add(executor);
                    listener.protocolActivated(executor);
                }
                return;
            }
        }

        // System.out.println("NEW PROTOCOL: " + message.getName());

        // Match against new conversations
        Protocol protocol = library.retrieveProtocol(message);
        if (protocol == null)
            throw new InvalidProtocolException("Cannot Match Message: " + message.getName() + " to a Protocol");

        // System.out.println("ABOUT TO MESS UP...");
        ProtocolExecutor executor = new ProtocolExecutor(name, protocol, transport);
        // System.out.println("GOT THE PROTOCOL...");
        executor.setRoleByMessage(message);
        executors.add(executor);
        executor.receive(message);
        if (executor.isActiveRole()) {
            active.add(executor);
            listener.protocolActivated(executor);
        }
    }
}
