package astra.bspl;

import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import astra.bspl.model.Message;
import astra.bspl.model.MessageSchema;
import astra.bspl.model.Parameter;

public class Utils {
    private static ObjectMapper mapper = new ObjectMapper();
    
    public static MessageSchema matchToSchema(List<MessageSchema> candidates, Message message) {
        for (MessageSchema pattern : candidates) {

            if (message.getName().equals(pattern.getName()) && message.getSender().equals(pattern.getSender())) {
                boolean matched = true;

                for (Parameter parameter : pattern.getParameters()) {
                    if (!message.getParameters().contains(parameter.getName())) {
                        matched = false;
                    }
                }
                if (matched) {
                    return pattern;
                }
            }
        }
        return null;
    }

    public static String toJsonString(Message message) {
        try {
            return mapper.writeValueAsString(message);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "Could not transform.";
    }

    public static String toJsonString(List<MessageSchema> schemas) {
        try {
            return mapper.writeValueAsString(schemas);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return "Could not transform.";
    }
}
