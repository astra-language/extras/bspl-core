package astra.bspl.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ParticipantSet {
    private Map<String, String> participants = Collections.synchronizedMap(new HashMap<>());
    
    public ParticipantSet set(String key, String value) {
        participants.put(key, value);
        return this;
    }

    public String get(String key) {
        return participants.get(key);
    }

    public boolean contains(String key) {
        return participants.containsKey(key);
    }

    public Map<String, String> getParticipants() {
        return participants;
    }

    public void setParticipants(HashMap<String, String> parameters) {
        this.participants = parameters;
    }

    public String toString() {
        return participants.toString();
    }    
}
