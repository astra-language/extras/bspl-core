package astra.bspl.model;

public class Parameter {
    public static final String OUT = "out";
    public static final String IN = "in";
    public static final String NIL = "nil";
    public static final String ANY = "any";
    public static final String OPT = "opt";
    
    private String type;
    private String name;
    private boolean key;

    public Parameter(String type, String name) {
        this(type, name, false);
    }

    public Parameter(String type, String name, boolean key) {
        this.type = type;
        this.name = name;
        this.key = key;
    }

    public Parameter() {}

    public boolean isType(String type) {
        return this.type.equals(type);
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isKey() {
        return key;
    }

    public void setKey(boolean key) {
        this.key = key;
    }

    
}
