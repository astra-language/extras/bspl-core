package astra.bspl.model;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class ParameterSet implements java.io.Serializable {
    private Map<String, String> parameters = Collections.synchronizedMap(new HashMap<>());
    
    public ParameterSet set(String key, String value) {
        parameters.put(key, value);
        return this;
    }

    public String get(String key) {
        return parameters.get(key);
    }

    public String get(String key, String defaultValue) {
        return parameters.getOrDefault(key, defaultValue);
    }

    public boolean contains(String key) {
        return parameters.containsKey(key);
    }

    public Map<String, String> getParameters() {
        return parameters;
    }

    public void setParameters(HashMap<String, String> parameters) {
        this.parameters = parameters;
    }    

    public String toString() {
        return parameters.toString();
    }
}
