package astra.bspl.model;

public class Message implements java.io.Serializable {
    private String from;
    private String to;
    private String sender;
    private String receiver;
    private String name;
    private ParameterSet parameters = new ParameterSet();

    public Message(String from, String to, String sender, String receiver, String name) {
        this.from = from;
        this.to = to;
        this.sender = sender;
        this.receiver = receiver;
        this.name = name;
    }

    public Message() {}

    public Message addParameter(String key, String value) {
        parameters.set(key, value);
        return this;
    }

    public String getFrom() {
        return from;
    }

    public String getTo() {
        return to;
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public ParameterSet getParameters() {
        return parameters;
    }

    public void setParameters(ParameterSet parameters) {
        this.parameters = parameters;
    }

    
}
