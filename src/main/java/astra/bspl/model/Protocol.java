package astra.bspl.model;

import java.util.LinkedList;
import java.util.List;

import astra.bspl.InvalidMessageException;

public class Protocol {
    private String name;
    private List<String> roles = new LinkedList<String>();
    private List<Parameter> parameters = new LinkedList<Parameter>();
    private List<String> privateParameters = new LinkedList<String>();
    private List<MessageSchema> schemas = new LinkedList<MessageSchema>(); //MessagePattern == MessageSchema

    public Protocol(String name) {
        this.name = name;
    }

    public Protocol() { }

    public Protocol addRole(String role) {
        roles.add(role);
        return this;
    }

    public Protocol addParameter(Parameter parameter) {
        parameters.add(parameter);
        return this;
    }

    public Protocol addPrivateParameter(String parameter) {
        privateParameters.add(parameter);
        return this;
    }

    public Protocol addMessageSchema(MessageSchema message) {
        if (!roles.contains(message.getSender())) {
            throw new InvalidMessageException("Invalid Sender: " + message.getSender());
        }
        if (!roles.contains(message.getReceiver())) {
            throw new InvalidMessageException("Invalid Reciever: " + message.getReceiver());
        }

        for (Parameter parameter : message.getParameters()) {
            if (!matchingParameter(parameter)) {
                throw new InvalidMessageException("Invalid Parameter: " + parameter.getName());
            }
        }
        schemas.add(message);
        return this;
    }

    private boolean matchingParameter(Parameter param) {
        // Match against parameter list
        for (Parameter parameter : parameters) {
            if (parameter.getName().equals(param.getName()))
                return true;
        }

        // Now try to match against private parameters
        for (String privateParameter : privateParameters) {
            if (param.getName().equals(privateParameter))
                return true;
        }

        return false;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getRoles() {
        return roles;
    }

    public void setRoles(List<String> roles) {
        this.roles = roles;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    public List<String> getPrivateParameters() {
        return privateParameters;
    }

    public void setPrivateParameters(List<String> privateParameters) {
        this.privateParameters = privateParameters;
    }

    public List<MessageSchema> getSchemas() {
        return schemas;
    }

    public void setSchemas(List<MessageSchema> messages) {
        this.schemas = messages;
    }

    
}