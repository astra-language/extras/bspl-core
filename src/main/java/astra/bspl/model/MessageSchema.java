package astra.bspl.model;

import java.util.LinkedList;
import java.util.List;

public class MessageSchema {
    private String sender;
    private String receiver;
    private String name;

    private List<Parameter> parameters = new LinkedList<Parameter>();

    public MessageSchema(String sender, String receiver) {
        this.sender = sender;
        this.receiver = receiver;
    }

    public MessageSchema() {}

    public MessageSchema addParameter(Parameter parameter) {
        parameters.add(parameter);
        return this;
    }

    public MessageSchema addName(String name) {
        this.name = name;
        return this;
    }

    public Message createMessage(String from, String to) {
        return new Message(from, to, sender, receiver, name);
    }

    public String getSender() {
        return sender;
    }

    public void setSender(String sender) {
        this.sender = sender;
    }

    public String getReceiver() {
        return receiver;
    }

    public void setReceiver(String receiver) {
        this.receiver = receiver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Parameter> getParameters() {
        return parameters;
    }

    public void setParameters(List<Parameter> parameters) {
        this.parameters = parameters;
    }

    
}
