package astra.bspl;

import java.util.LinkedList;
import java.util.List;

import astra.bspl.model.Message;
import astra.bspl.model.MessageSchema;
import astra.bspl.model.Parameter;
import astra.bspl.model.ParameterSet;
import astra.bspl.model.ParticipantSet;
import astra.bspl.model.Protocol;
import astra.bspl.transport.ProtocolTransport;

public class ProtocolExecutor {
    private Protocol protocol;
    private String role;
    private String name;
    private ProtocolTransport transport;
    private ParticipantSet participants = new ParticipantSet();

    private ParameterSet parameters = new ParameterSet();
    private LinkedList<Message> messages = new LinkedList<>();
    private ParameterSet outParameters = new ParameterSet();

    public ProtocolExecutor(String name, String role, ParticipantSet participants, Protocol protocol, ProtocolTransport transport) {
        this.protocol = protocol;
        this.transport = transport;
        this.participants = participants;
        this.role = role;
        this.name = name;
    }

    
    public ProtocolExecutor(String name, Protocol protocol, ProtocolTransport transport) {
        this.protocol = protocol;
        this.transport = transport;
        this.name = name;
    }

    public void setRoleByMessage(Message message) {
        role = message.getReceiver();
    }

    public String activeRole() {
        return messages.isEmpty() ? protocol.getSchemas().get(0).getSender():messages.getLast().getReceiver();
    }

    private boolean hasMessage(String name) {
        for (Message message: messages) {
            if (message.getName().equals(name)) return true;
        }
        return false;
    }
    
    public List<MessageSchema> getCandidateSenderSchemas(String theRole) {
        List<MessageSchema> active = new LinkedList<MessageSchema>();

        for (MessageSchema schema : protocol.getSchemas()) {
            // Remove Schemas Matched to Messages
            if (schema.getSender().equals(theRole) && !hasMessage(schema.getName()))
                active.add(schema);
        }
        return active;
    }

    public List<MessageSchema> getCandidateReceiverSchemas(Message message) {
        List<MessageSchema> active = new LinkedList<MessageSchema>();

        for (MessageSchema schema : protocol.getSchemas()) {
            // Remove Schemas Matched to Messages
            if (schema.getSender().equals(message.getSender()) &&
                schema.getReceiver().equals(message.getReceiver()))
                active.add(schema);
        }
        return active;
    }

    public String getRole() {
        return role;
    }
    
    public MessageSchema getSchema(String name) {
        for (MessageSchema schema : protocol.getSchemas()) {
            if (schema.getName().equals(name)) return schema;
        }
        return null;
    }

    public ParameterSet getParameters() {
        return parameters;
    }

    public ParticipantSet getParticipants() {
        return participants;
    }

    public Message getLastMessage() {
        return messages.isEmpty() ? null:messages.getLast();
    }

    public Message createMessage(String type, ParameterSet set) {
        if (!role.equals(activeRole())) {
            throw new InvalidMessageException("Incorrect Message Sequence for protocol: " + protocol.getName() 
                        + "; Message: " + type + " is not yet allowed for role: "+ role);
        }

        List<MessageSchema> patterns = getCandidateSenderSchemas(role);
        for (MessageSchema pattern : patterns) {
            if (pattern.getName().equals(type)) {
                Message message = pattern.createMessage(name, participants.get(pattern.getReceiver()));
                for (Parameter parameter : pattern.getParameters()) {
                    if (parameter.isType(Parameter.IN)) {
                        String value = parameters.get(parameter.getName());
                        if (value == null) 
                            throw new InvalidMessageException("Missing Parameter value for: " + parameter.getName() + " in message: " + message.getName() + " while playing role: " + message.getSender());
                        message.addParameter(parameter.getName(), value);
                    } else if (parameter.isType(Parameter.OUT)) {
                        String value = set.get(parameter.getName());
                        if (value == null) 
                            throw new InvalidMessageException("Missing Parameter value for: " + parameter.getName() + " in message: " + message.getName());
                        message.addParameter(parameter.getName(),value);
                        parameters.set(parameter.getName(), value);
                    } else {
                        throw new InvalidMessageException("Parameter type not supported: " + parameter.getName() + " for message: " + message.getName());
                    }
                }

                // Move on to the next set of patterns...
                // index += patterns.size();
                messages.add(message);

                return message;
            }
        }
        return null;
    }

    public boolean receive(Message message) {
        System.out.println("["+name+"] RECEIVED: " + message.getName());
        System.out.println(Utils.toJsonString(message));
        MessageSchema pattern = Utils.matchToSchema(getCandidateReceiverSchemas(message), message);
        if (pattern != null) {
            // Add OUT data to local data store & confirm the IN data has not been
            // changed...
            for (Parameter parameter : pattern.getParameters()) {
                String value = message.getParameters().get(parameter.getName());
                if (parameter.isType(Parameter.OUT)) {
                    parameters.set(parameter.getName(), value);
                } else if (parameter.isType(Parameter.IN)) {
                    String expectedValue = parameters.get(parameter.getName());
                    if (expectedValue == null) {
                        parameters.set(parameter.getName(),value);
                    } else if (!value.equals(expectedValue)) {
                        throw new InvalidMessageException("Parameter Mismatch: " + parameter.getName() + " has value: '"  + value + "' but: '" + expectedValue + "' was expected");
                    }
                }
            }
            participants.set(message.getSender(),message.getFrom());
            messages.add(message);
            return true;
        }
        return false;
    }

    public boolean isFinished() {
        return false;
    }

    public List<Message> messages() {
        return messages;
    }

    public boolean isActiveRole() {
        return activeRole().equals(role);
    }

    public Protocol getProtocol() {
        return protocol;
    }

    public void send(Message message) {
        transport.send(message);
    }

    public ParameterSet getOutParameters() {
        return outParameters;
    }

    public void resetOutParameters() {
        outParameters = new ParameterSet();
    }
}
