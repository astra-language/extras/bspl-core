import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

import astra.bspl.InvalidMessageException;
import astra.bspl.ProtocolExecutor;
import astra.bspl.Utils;
import astra.bspl.model.Message;
import astra.bspl.model.MessageSchema;
import astra.bspl.model.Parameter;
import astra.bspl.model.ParameterSet;
import astra.bspl.model.ParticipantSet;
import astra.bspl.model.Protocol;
import astra.bspl.transport.LocalProtocolTransport;
import astra.bspl.transport.ProtocolTransport;

public class PurchaseTest {
    Protocol protocol = new Protocol("Purchase")
        .addRole("Buyer")
        .addRole("Seller")
        .addRole("Shipper")
        .addParameter(new Parameter(Parameter.OUT, "id", true))
        .addParameter(new Parameter(Parameter.OUT, "item"))
        .addParameter(new Parameter(Parameter.OUT, "price"))
        .addParameter(new Parameter(Parameter.OUT, "outcome"))
        .addPrivateParameter("address")
        .addPrivateParameter("resp")
        .addPrivateParameter("shipped")
        .addMessageSchema(
            new MessageSchema("Buyer", "Seller")
                .addName("rfq")
                .addParameter(new Parameter(Parameter.OUT, "id", true))
                .addParameter(new Parameter(Parameter.OUT, "item")))
        .addMessageSchema(
            new MessageSchema("Seller", "Buyer")
                .addName("quote")
                .addParameter(new Parameter(Parameter.IN, "id"))
                .addParameter(new Parameter(Parameter.IN, "item"))
                .addParameter(new Parameter(Parameter.OUT, "price")))
        .addMessageSchema(
            new MessageSchema("Buyer", "Seller")
                .addName("accept")
                .addParameter(new Parameter(Parameter.IN, "id", true))
                .addParameter(new Parameter(Parameter.IN, "item"))
                .addParameter(new Parameter(Parameter.IN, "price"))
                .addParameter(new Parameter(Parameter.OUT, "address"))
                .addParameter(new Parameter(Parameter.OUT, "resp")))
        .addMessageSchema(
            new MessageSchema("Buyer", "Seller")
                .addName("reject")
                .addParameter(new Parameter(Parameter.IN, "id", true))
                .addParameter(new Parameter(Parameter.IN, "item"))
                .addParameter(new Parameter(Parameter.IN, "price"))
                .addParameter(new Parameter(Parameter.OUT, "outcome"))
                .addParameter(new Parameter(Parameter.OUT, "resp")))
        .addMessageSchema(
            new MessageSchema("Seller", "Shipper")
                .addName("ship")
                .addParameter(new Parameter(Parameter.IN, "id", true))
                .addParameter(new Parameter(Parameter.IN, "item"))
                .addParameter(new Parameter(Parameter.IN, "address"))
                .addParameter(new Parameter(Parameter.OUT, "shipped")))
        .addMessageSchema(
            new MessageSchema("Shipper", "Seller")
                .addName("deliver")
                .addParameter(new Parameter(Parameter.IN, "id", true))
                .addParameter(new Parameter(Parameter.IN, "item"))
                .addParameter(new Parameter(Parameter.IN, "address"))
                .addParameter(new Parameter(Parameter.OUT, "outcome")));


    private static ProtocolTransport transport = new LocalProtocolTransport();
    
    @Test
    public void positiveTest() {
        ProtocolExecutor seller = new ProtocolExecutor("seller", "Seller",
            new ParticipantSet().set("Buyer", "buyer"), 
            protocol, transport);
        ProtocolExecutor buyer = new ProtocolExecutor("buyer", "Buyer",
            new ParticipantSet().set("Seller", "seller"), 
            protocol, transport);

        Message message = buyer.createMessage("rfq", 
            new ParameterSet()
                .set("id", "1")
                .set("item", "apples"));
        assertNotNull(message);

        assertTrue(seller.receive(message));

        message = seller.createMessage("quote", new ParameterSet().set("price", "20"));
        assertNotNull(message);

        assertTrue(buyer.receive(message));

        message = buyer.createMessage("accept", new ParameterSet().set("address","home").set("resp", "yipee!"));
        assertNotNull(message);

        assertTrue(seller.receive(message));
    }

    @Test(expected=InvalidMessageException.class)
    public void missingParameterTest() {
        // ProtocolExecutor seller = new ProtocolExecutor("Seller", protocol);
        ProtocolExecutor buyer = new ProtocolExecutor("buyer", "Buyer", 
            new ParticipantSet().set("Seller", "seller"), 
            protocol, transport);
        // ProtocolExecutor shipper = new ProtocolExecutor("Shipper", protocol);

       buyer.createMessage("rfq", 
            new ParameterSet()
                .set("item", "apples"));

    }

    @Test(expected=InvalidMessageException.class)
    public void changedDataTest() {
        ProtocolExecutor seller = new ProtocolExecutor("seller", "Seller", 
            new ParticipantSet().set("Buyer", "buyer"), 
            protocol, transport);
        ProtocolExecutor buyer = new ProtocolExecutor("buyer", "Buyer",
            new ParticipantSet().set("Seller", "seller"), 
            protocol, transport);
    // ProtocolExecutor shipper = new ProtocolExecutor("Shipper", protocol);

        Message message = buyer.createMessage("rfq", 
            new ParameterSet()
                .set("id", "1")
                .set("item", "apples"));

        seller.receive(message);

        message = seller.createMessage("quote", new ParameterSet().set("price", "20"));
        message.getParameters().set("id", "2");
        buyer.receive(message);
    }

    @Test //(expected=InvalidMessageException.class)
    public void shipperTest() {
        ProtocolExecutor seller = new ProtocolExecutor("seller", "Seller", 
            new ParticipantSet().set("Buyer", "buyer").set("Shipper", "shipper"), 
            protocol, transport);
            ProtocolExecutor buyer = new ProtocolExecutor("buyer", "Buyer",
            new ParticipantSet().set("Seller", "seller"), 
            protocol, transport);
        ProtocolExecutor shipper = new ProtocolExecutor("shipper", "Shipper", new ParticipantSet(),
            // new ParticipantSet().set("Seller", "seller"), 
            protocol, transport);

        Message message = buyer.createMessage("rfq", 
            new ParameterSet()
                .set("id", "1")
                .set("item", "apples"));

        seller.receive(message);

        message = seller.createMessage("quote", new ParameterSet().set("price", "20"));
        assertTrue(buyer.receive(message));

        message = buyer.createMessage("accept", new ParameterSet().set("address", "my house").set("resp","?"));
        assertTrue(seller.receive(message));

        message = seller.createMessage("ship", new ParameterSet().set("shipped", "no"));
        assertTrue(shipper.receive(message));

        message = shipper.createMessage("deliver", new ParameterSet().set("outcome", "1"));
        assertTrue(shipper.receive(message));
    }
}
