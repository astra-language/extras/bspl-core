import static org.junit.Assert.assertNotNull;

import org.junit.Test;

import astra.bspl.ProtocolExecutor;
import astra.bspl.Utils;
import astra.bspl.model.Message;
import astra.bspl.model.MessageSchema;
import astra.bspl.model.Parameter;
import astra.bspl.model.ParameterSet;
import astra.bspl.model.ParticipantSet;
import astra.bspl.model.Protocol;
import astra.bspl.transport.LocalProtocolTransport;
import astra.bspl.transport.ProtocolTransport;

public class BasicProtocolTest {
    Protocol protocol = new Protocol("BasicProtocol")
        .addRole("A")
        .addRole("B")
        .addParameter(new Parameter(Parameter.OUT, "id", true))
        .addParameter(new Parameter(Parameter.OUT, "data"))
        .addMessageSchema(
            new MessageSchema("A", "B")
                .addName("test")
                .addParameter(new Parameter(Parameter.OUT, "id", true))
                .addParameter(new Parameter(Parameter.OUT, "data")));

    private static ProtocolTransport transport = new LocalProtocolTransport();

    @Test
    public void protocolTest() {

        ProtocolExecutor executor1 = new ProtocolExecutor(
            "e1","A", 
            new ParticipantSet().set("e2", "B"), 
            protocol, transport);
        ProtocolExecutor executor2 = new ProtocolExecutor(
            "e2", "B", 
            new ParticipantSet().set("e1", "A"), 
            protocol, transport);

        Message message = executor1.createMessage("test", 
            new ParameterSet()
                .set("id", "1")
                .set("data", "test"));

        assertNotNull(Utils.matchToSchema(executor2.getCandidateSenderSchemas(executor2.activeRole()), message));
    }
}
