import org.junit.Test;

import astra.bspl.ProtocolAdaptor;
import astra.bspl.ProtocolExecutor;
import astra.bspl.ProtocolLibrary;
import astra.bspl.ProtocolListener;
import astra.bspl.Utils;
import astra.bspl.model.Message;
import astra.bspl.model.ParameterSet;
import astra.bspl.model.ParticipantSet;
import astra.bspl.transport.LocalProtocolTransport;
import astra.bspl.transport.ProtocolTransport;

public class AdaptorTest {
    private static ProtocolLibrary library = new ProtocolLibrary()
        .loadProtocol("examples/purchase.json");

    private static ProtocolTransport transport = new LocalProtocolTransport();
 
    @Test
    public void adaptorTest() throws Exception {
        ProtocolAdaptor seller = new ProtocolAdaptor("seller", library, transport).setListener(new ProtocolListener() {
            @Override
            public void protocolEnacted(ProtocolExecutor executor) {
                if (!executor.isActiveRole()) return;

                if (executor.getProtocol().getName().equals("Purchase")) {
                    Message message = executor.createMessage("rfq", 
                        new ParameterSet()
                            .set("id", "1")
                            .set("item", "apples"));
                    executor.send(message);
                }
            }

            @Override
            public void protocolActivated(ProtocolExecutor executor) {
                // Message lastMessage = executor.getLastMessage();
                // System.out.println(Utils.toJsonString(lastMessage));
                if (executor.getProtocol().getName().equals("Purchase")) {
                    Message message = executor.createMessage("quote", 
                        new ParameterSet()
                            .set("price", "10"));
                    if (message == null) {
                        System.out.println("Could not create response: quote " + " for: "+executor.getProtocol().getName());
                    } else {
                        executor.send(message);
                    }
                }
            }
        });

        ProtocolAdaptor buyer = new ProtocolAdaptor("buyer", library, transport).setListener(new ProtocolListener() {
            @Override
            public void protocolEnacted(ProtocolExecutor executor) {
                // System.out.println("in protocolEnacted()");
                if (!executor.isActiveRole()) return;

                // System.out.println("protocol: " + executor.getProtocol().getName());
                if (executor.getProtocol().getName().equals("Purchase")) {
                    Message message = executor.createMessage("rfq", 
                        new ParameterSet()
                            .set("id", "1")
                            .set("item", "apples"));


                    // System.out.println("Message to: " + message.getTo());
                    executor.send(message);
                }
            }

            @Override
            public void protocolActivated(ProtocolExecutor executor) {
                // Message lastMessage = executor.getLastMessage();
                // System.out.println(Utils.ring(lastMessage));

                if (executor.getProtocol().getName().equals("Purchase")) {
                }
            }
        });

        ProtocolExecutor executor = buyer.enactProtocol("Purchase", "Buyer", new ParticipantSet().set("Seller", "seller"));

    }

}
